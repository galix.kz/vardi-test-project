from django.conf.urls import url

from Realtime import consumers

websocket_urlpatterns = [
    url(r'^ws/chat/(?P<group_id>[^/]+)/$', consumers.TestConsumer),
]