import asyncio
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json
from django.core.cache import cache

from Test.models import UserTest, TestHistory


class TestConsumer(WebsocketConsumer):
    def group_send_message(self, message):
        async_to_sync(self.channel_layer.group_send)(
            self.test_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    def next_question(self, prev_right_ans_count, all_question_no,user):
        right_ans_count = 0
        if prev_right_ans_count is not None:
            right_ans_count = prev_right_ans_count
        cache.set('user_%s_group_%s_right_answers_count' % (user.username, self.test_group_name),
                  right_ans_count + 1)
        question_no = cache.get('group_%s_question_no' % self.test_group_name)

        if all_question_no == question_no + 1:
            self.group_send_message({'event': 'finish_test', 'message': {}})

        cache.set('group_%s_question_no' % self.test_group_name, question_no + 1)
        self.group_send_message({'event': 'next_question', 'message': ''})

    def connect(self):
        #
        # from django_redis import get_redis_connection
        # get_redis_connection("default").flushall()
        self.group_id = self.scope['url_route']['kwargs']['group_id']
        self.test_group_name = 'test_%s' % self.group_id
        # Join room group
        user = self.scope['user']
        user_test = UserTest.objects.all().filter(id=self.group_id).first()
        user_test.users.add(user)
        users_in_room = cache.get('users_in_%s' % self.test_group_name)
        users_in_room_list = []
        if users_in_room is not None:
            users_in_room_list = users_in_room
        if user.username not in users_in_room_list:
            users_in_room_list.append(user.username)
            cache.set('users_in_%s' % self.test_group_name, users_in_room_list)
        async_to_sync(self.channel_layer.group_add)(
            self.test_group_name,
            self.channel_name
        )
        self.group_send_message({'event': 'new_user_connect', 'message': {'users': users_in_room_list}})
        self.accept()

    def disconnect(self, close_code):
        # Leave room groupe
        users_in_room = cache.get('users_in_%s' % self.test_group_name)
        user_test = UserTest.objects.all().filter(id=self.group_id).first()
        user_test.users.remove(self.scope['user'])
        # print(users_in_room.remove(self.scope['user'].username))
        cache.set('users_in_%s' % self.test_group_name, users_in_room)
        self.group_send_message({'event': 'new_user_disconnect', 'message': {'users': users_in_room}})
        async_to_sync(self.channel_layer.group_discard)(
            self.test_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        message_type = text_data_json['event']
        user = self.scope['user']
        question_no = cache.get('group_%s_question_no' % self.test_group_name)
        prev_right_ans_count = cache.get('user_%s_group_%s_right_answers_count' % (user.username, self.test_group_name))
        if message_type == 'question_no':
            print(question_no)
            user_test = UserTest.objects.all().filter(id=self.group_id).first()
            # test = user_test.group.test_set.all().filter(id=question_no + 1)
            # print(test)
            self.send(
                text_data=json.dumps({'message': {'event': 'question_no', 'message': {'question_no': question_no}}}))

        if message_type == 'next_question':
            all_question_no = message['all_question_no']
            self.next_question(prev_right_ans_count, all_question_no,user)

        if message_type == 'get_right_answers_count':
            all_question_no = message['all_question_no']
            if prev_right_ans_count is None:
                prev_right_ans_count = 0
                cache.set('user_%s_group_%s_right_answers_count' % (user.username, self.test_group_name), 0)

            right_ans_count = prev_right_ans_count
            false_ans_count = question_no - right_ans_count
            if all_question_no == question_no:
                user_test = UserTest.objects.all().filter(id=self.group_id).first()
                print('asdasd')
                TestHistory.objects.create(user=user, user_test=user_test, right_answers=right_ans_count,
                                           false_answers=false_ans_count)
                winner_list = cache.get('winner_list_group_%s'%self.group_id)
                if winner_list is None:
                    winner_list = []
                winner_list.append({'user':user.username,'right_ans':right_ans_count})
                cache.set('winner_list_group_%s'%self.group_id,winner_list)
                self.group_send_message({'event':'get_winners_list','message':{'winners': winner_list}})
                user_test.active=False
                user_test.save()
            self.send(text_data=json.dumps({'message': {'event': 'get_right_answers_count',
                                                        'message': {'right_answers_count': right_ans_count,
                                                                    'false_answers_count': false_ans_count}
                                                        }}))
        if message_type == 'start_test':
            user_test = UserTest.objects.all().filter(id=self.group_id).first()
            if user_test.users.count() < 2:
                self.group_send_message({'event':'not_enough_people'})
            else:
                cache.set('group_%s_question_no' % self.test_group_name, 0, timeout=1000)
                self.group_send_message(message=text_data_json)


    # Receive message from room group
    def chat_message(self, event):
        message = event['message']
        # print(event)
        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message
        }))
