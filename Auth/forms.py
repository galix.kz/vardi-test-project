from django import forms
from django.contrib.auth import authenticate, get_user_model, login, logout

from Auth.models import MyUser

User = get_user_model()


class UserLoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")

        user = authenticate(username=username, password=password)

        if not user:
            print('asdqwe')
            raise forms.ValidationError("The user does not exist")

        if not user.check_password(password):
            print('qweqwe')
            raise forms.ValidationError("Incorrect password")

        return super(UserLoginForm, self).clean(*args, **kwargs)


class UserRegistrationForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = [
            'username',
            'password',
            'email'
        ]

