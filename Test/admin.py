from django.contrib import admin
from .models import TestGroup, Test, UserTest, TestHistory

admin.site.register(Test)
admin.site.register(TestGroup)
admin.site.register(UserTest)
admin.site.register(TestHistory)