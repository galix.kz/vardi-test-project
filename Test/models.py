from django.db import models


class TestGroup(models.Model):
    name = models.CharField(max_length=300)

    def __str__(self):
        return self.name


class Test(models.Model):
    test_group = models.ForeignKey('Test.TestGroup', on_delete=models.CASCADE)
    question = models.CharField(max_length=500)
    true_answer = models.CharField(max_length=500)
    false_answer1 = models.CharField(max_length=500)
    false_answer2 = models.CharField(max_length=500)
    false_answer3 = models.CharField(max_length=500)
    timer = models.TimeField()

    def __str__(self):
        return self.test_group.name + ' ' + self.question


class TestHistory(models.Model):
    user = models.ForeignKey('Auth.MyUser', blank=True, null=True, on_delete=models.CASCADE)
    test_date = models.DateTimeField(auto_now_add=True, auto_now=False)
    user_test = models.ForeignKey('Test.UserTest', on_delete=models.CASCADE,null=True)
    right_answers = models.IntegerField(default=0)
    false_answers = models.IntegerField(default=0)


class UserTest(models.Model):
    group = models.ForeignKey('Test.TestGroup', on_delete=models.CASCADE, related_name='test_group')
    users = models.ManyToManyField('Auth.MyUser', blank=True)
    test_date = models.DateTimeField(auto_now_add=True, auto_now=False)
    active = models.BooleanField(default=True)

    def __str__(self):
        return '%s %s' % (self.group.name, self.test_date)
