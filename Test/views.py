import json

from django.core import serializers
from django.shortcuts import render
from django.utils.safestring import mark_safe
from django.views import View
from django.views.generic import DetailView, CreateView

from Test.models import UserTest


class IndexView(View):

    def get(self, request, *args, **kwargs):
        return render(request, 'index.html', {"title": "Index"})


class CreateTestView(CreateView):
    model = UserTest
    fields = ['group']
    template_name = 'forms.html'

    def get_success_url(self, **kwargs):
        return '/test/search/%s' % self.object.id


class SearchTestListView(View):
    queryset = UserTest.objects.all().filter(active=True)
    template_name = 'search.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {"queryset": self.queryset, "title": "Search Test"})


class SearchTestDetailView(DetailView):
    model = UserTest
    template_name = 'search_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pk = self.kwargs.get(self.pk_url_kwarg)
        # if user_test.users.filter(id=self.request.user.id).first() is None:
        #     user_test.users.add(self.request.user)
        #     user_test.save()
        return context


class StartTestView(View):

    def get(self, request, *args, **kwargs):
        print(kwargs['group_id'])
        group_id = kwargs['group_id']
        user_test = UserTest.objects.all().filter(id=group_id).first()
        question_set = user_test.group.test_set.all()
        question_set_serialize = serializers.serialize('json', question_set)
        print(question_set_serialize)
        return render(request, 'start.html', {'group_id_json': group_id, 'active': user_test.active,
                                              'question_set': mark_safe(json.dumps(question_set_serialize))})
